﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.API.State.Request
{
    public class StateForInsert
    {
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
    }
}
