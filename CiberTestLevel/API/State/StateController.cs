﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using CiberTestLevel.Infraestructure.Repository.StateRepository;
using CiberTestLevel.API.State.Request;
using CiberTestLevel.Domain;

namespace CiberTestLevel.API.State
{
    [Produces("application/json")]
    [Route("api/State")]
    public class StateController : Controller
    {
        private readonly IStateRepository _StareRepo;

        public StateController(IStateRepository stateRepo)
        {
            _StareRepo = stateRepo;
        }


        [HttpGet]
        [Route("GetState")]
        public async Task<IActionResult> GetState([FromQuery] Guid Id)
        {
            var state = await _StareRepo.GetState(Id);
            if (state == null)
            {
                return NotFound("State not found");
            }
            else
            {
                return Ok(state);
            }
        }


        [HttpPost]
        [Route("InsertState")]
        public async Task<IActionResult> InsertState([FromBody]StateForInsert stateToInsert)
        {
            var state = new StateEntity
            {
                City = stateToInsert.City,
                State = stateToInsert.State,
                Zip = stateToInsert.Zip
            };

            await _StareRepo.AddAsync(state);
            if (await _StareRepo.SaveChangesAsync())
            {
                return Ok(state);
            }
            else
            {
                return BadRequest("Error");
            }
        }
    }
}
