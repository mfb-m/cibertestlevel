﻿using CiberTestLevel.API.State.Request;
using CiberTestLevel.Domain;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.API.State.Validations
{
    public class InsertStateValidator : AbstractValidator<StateForInsert>
    {
        //no me lío más pongo lo básico, pero con fluent se hacen maravillas :-)
        //postdata pongo notEmpty string y Int son tipos non nullables.... a menos que pongas ? elvis, en fin harina de otro costal
        public InsertStateValidator()
        {
            RuleFor(o => o.City).NotEmpty();
            RuleFor(o => o.State).NotEmpty();
            RuleFor(o => o.Zip).NotEmpty();
        }
    }
}
