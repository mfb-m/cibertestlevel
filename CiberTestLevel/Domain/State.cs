﻿using CiberTestLevel.Domain.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Domain
{
    public class StateEntity : EntityBase
    {
        public string City { get; set; }
        public string State { get; set; }
        public int Zip { get; set; }
    }
}
