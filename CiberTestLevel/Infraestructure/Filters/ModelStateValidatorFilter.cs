﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Infraestructure.Filters
{
    public class ModelStateValidatorFilter : ActionFilterAttribute
    {
        //Este filtro evita lo cansino y feo que puede ser, que en cada método del controlador comprobemos el modelo y devolvamos el error
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                context.Result = new BadRequestObjectResult(context.ModelState);
            }
        }
    }
}
