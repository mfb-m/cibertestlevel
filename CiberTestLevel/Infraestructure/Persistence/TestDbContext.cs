﻿using CiberTestLevel.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Infraestructure.Persistence
{
    public class TestDbContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseInMemoryDatabase("TestState")
                .ConfigureWarnings
                (a => a.Throw(RelationalEventId.QueryClientEvaluationWarning));
        }



        public DbSet<StateEntity> State { get; set; }
    }
}
