﻿using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Infraestructure.Repository
{
    public interface IRepositoryBase
    {
        Task<EntityEntry> AddAsync<TEntity>(TEntity entity) where TEntity : class;
        Task<bool> SaveChangesAsync();
        void Remove<TEntity>(TEntity entity) where TEntity : class;
        void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class;
    }
}
