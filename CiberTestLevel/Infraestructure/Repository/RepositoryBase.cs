﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Infraestructure.Repository
{
    public class RepositoryBase<TDbContext> : IRepositoryBase where TDbContext : DbContext
    {
        protected readonly TDbContext _dbContext;

        public RepositoryBase(TDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<EntityEntry> AddAsync<TEntity>(TEntity entity) where TEntity : class
        {
            return await _dbContext.AddAsync(entity);
        }

        public async Task<bool> SaveChangesAsync()
        {
            return await _dbContext.SaveChangesAsync() > 0;
        }

        public void Remove<TEntity>(TEntity entity) where TEntity : class
        {
            _dbContext.Remove(entity);
        }

        public void RemoveRange<TEntity>(IEnumerable<TEntity> entities) where TEntity : class
        {
            _dbContext.RemoveRange(entities);
        }
    }
}
