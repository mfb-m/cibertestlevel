﻿using CiberTestLevel.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CiberTestLevel.Infraestructure.Repository.StateRepository
{
    public interface IStateRepository : IRepositoryBase
    {
        Task<StateEntity> GetState(Guid Id);
    }
}
