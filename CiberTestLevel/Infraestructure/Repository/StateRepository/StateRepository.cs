﻿using CiberTestLevel.Infraestructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CiberTestLevel.Domain;
using Microsoft.EntityFrameworkCore;

namespace CiberTestLevel.Infraestructure.Repository.StateRepository
{
    public class StateRepository : RepositoryBase<TestDbContext> , IStateRepository
    {
        public StateRepository(TestDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<StateEntity> GetState(Guid Id)
        {
            return await _dbContext.State.FirstOrDefaultAsync(o => o.Id == Id);
        }
    }
}
