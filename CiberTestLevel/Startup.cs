﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using CiberTestLevel.Infraestructure.Persistence;
using CiberTestLevel.Infraestructure.Repository.StateRepository;
using FluentValidation.AspNetCore;
using CiberTestLevel.Infraestructure.Filters;

namespace CiberTestLevel
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        
        public void ConfigureServices(IServiceCollection services)
        {
            //añadimos cords lo suyo es agregar políticas, pero esto es un ejemplo...
            services.AddCors();

            //añadimos el dbcontext y creamos una bbdd en memoria 
            services.AddDbContext<TestDbContext>()
                .AddEntityFrameworkInMemoryDatabase();

            //por inyección de dependencias incicamos que este repository se cree cada vez que hacemos una llamada
            services.AddScoped<IStateRepository, StateRepository>();

            //Añdimos el filtro de las validaciones con fluent validation de manera global :-), asi cada request el modelo es validado por su respectiva validación
            services.AddMvc(opt => opt.Filters.Add(typeof(ModelStateValidatorFilter))).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<Startup>());
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                //me encanta esto
                app.UseDeveloperExceptionPage();
            }

            //Ponemos como ejemplo que acepte todo, SOLO EN CASO DE EJEMPLO
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());

            app.UseMvc();
        }
    }
}
